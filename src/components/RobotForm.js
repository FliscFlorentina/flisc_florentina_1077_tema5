import React, {Component} from 'react';

class RobotForm extends Component
{
    constructor(props)
    {
        super(props)
        this.state={
            name:'',
            mass:'',
            type:''
        }
        
    }
    
     handlerChangeName=(event) =>
    {
        this.setState({
        name:event.target.value
        });
    }
     handlerChangeMass=(event) =>
    {
        this.setState({
        mass:event.target.value
        });
    }
     handlerChangeType=(event) =>
    {
        this.setState({
        type:event.target.value
        });
    }
    
    add=() =>
    {
        const robot=this.state;
        this.props.onAdd(robot);
    }
    
    render()
    {
        return(
            <div>
           <h1>AddRobot </h1>
           <input type="name" id="name" placeholder="Name" value={this.state.name} onChange={this.handlerChangeName}/>
           <input type="text" id="mass" placeholder="Mass" value={this.state.mass} onChange={this.handlerChangeMass}/>
           <input type="text" id="type" placeholder="Type" value={this.state.type} onChange={this.handlerChangeType}/>
           <button type="button" value="add" onClick={this.add}>Add</button>
           </div>);
    }
}

export default RobotForm